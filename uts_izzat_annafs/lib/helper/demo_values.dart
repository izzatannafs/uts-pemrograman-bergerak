class DemoValues {
  static final String userImage = "assets/images/user.jpg";
  static final String postImage = "assets/images/leaf2.jpg";
  static final String userName = "Izzat An nafs";
  static final String userEmail = "izzatannafs89@gmail.com";
  static final String postTime = "16 Desember, 2023";
  static final String postTitle = "Pemrograman Bergerak";
  static final String postSummary =
      "UTS Pemrograman Bergerak Izzat An nafs 210401010134";

  static var posts;

  static var users;
}
